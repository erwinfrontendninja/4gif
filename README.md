## Installation
### `npm install`
### `npm start` for running development mode, then open [http://localhost:3000](http://localhost:3000)
### `npm test` for running test
### `npm run build`

- search GIFs via keyword(s)
- show matching GIFs in a space optimized grid
- filter by random and trending GIF category
- infinite scrolling in search result
- responsive and accessible
- dark mode

I am using axios to get data from API and node-sass for compiling scss, because use css is so weird :D
