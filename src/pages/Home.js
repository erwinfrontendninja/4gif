import React from 'react';
import DarkModeToggle from './../components/DarkModeToggle';
import FilterToggle from './../components/FilterToggle';
import ListTrendingGifs from './../components/ListTrendingGifs';
import ListRandomGifs from './../components/ListRandomGifs';
import SearchBar from './../components/SearchBar';
import EventEmitter from './../Event';

export default class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      darkMode: false,
      isShow: false,
    };
    EventEmitter.on('filterToggle', value => this.setState({ isShow: value }));
  }

  render() {
    return (
      <div className="container">
        <div className="flex is-100 is-middle py-60">
          <div className="flex-item">
            <div className="text-center mb-30">
              <h1 className="u-str-26 mb-5">Why GIPHY?</h1>
              <p>As the first and largest GIF search engine that serves more than 7 billion GIFs per day, it’s safe to say GIPHY knows GIFs.</p>
              <DarkModeToggle />
            </div>
            <SearchBar location={ this.props.history } />
            <FilterToggle />
            {
              !this.state.isShow
              ? <ListTrendingGifs />
              : <ListRandomGifs />
            }
          </div>
        </div>
      </div>
    );
  }
}
