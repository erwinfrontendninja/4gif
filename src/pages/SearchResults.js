import React from 'react';
import ListSearchResult from './../components/ListSearchResult';
import SearchBar from './../components/SearchBar';

export default class SearchResult extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      q: this.getKeyword(),
    };
  }

  getKeyword() {
    const path = this.props.history.location.pathname;
    const keyword = path.split('/')[2].replace(/-/g, ' ');
    return keyword;
  }

  render() {
    return (
      <div className="container">
        <div className="flex py-60">
          <div className="flex-item">
            <SearchBar location={ this.props.history } />
            <h1 className="u-str-26 mb-5">Keyword: { this.getKeyword() }</h1>
            <ListSearchResult keyword={ this.getKeyword() } />
          </div>
        </div>
      </div>
    );
  }
}
