import React from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";
import Home from './pages/Home';
import SearchResult from './pages/SearchResults';
import EventEmitter from './Event';
import './App.scss';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      darkMode: false,
    };
    EventEmitter.on('toggleDarkMode', value => this.setState({ darkMode: value }));
  }

  render() {
    return (
      <main className={ this.state.darkMode ? 'is-dark' : 'is-light' }>
        <Router>
          <Route path="/" exact component={ Home } />
          <Route path="/search" component={ SearchResult } />
        </Router>
      </main>
    );
  }
}

export default App;
