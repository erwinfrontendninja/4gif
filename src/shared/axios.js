import axios from 'axios';

const api_key = 'FFKBmG9nxUycFMMb725v5GyrUgJt1bmE';
axios.defaults.headers.common = {
  'api_key': api_key,
  'Access-Control-Allow-Origin': '*'
};

axios.interceptors.response.use(response => response, (error) => {
  const { status } = error.response;
  if (status === 500) {
    // show error notification
  }

  if (status === 401) {
    // force logout
  }

  return Promise.reject(error);
});
