import React from 'react';
import EventEmitter from './../Event';

class DarkModeToggle extends React.Component {
  constructor(props) {
    super(props);
    this.state = { checked: '' };
    this.toggleAction = this.toggleAction.bind(this);
  }

  toggleAction(event) {
    this.setState({ checked: event.target.checked });
    EventEmitter.emit('toggleDarkMode', event.target.checked);
  }

  render() {
    return (
      <label className={ this.state.checked ? 'toggle is-checked' : 'toggle' }>
        <input type="checkbox" onChange={ this.toggleAction } hidden/>
        <div className="toggle-text">
          <span>Light</span>
          <span>Dark</span>
        </div>
      </label>
    );
  }
}

export default DarkModeToggle;