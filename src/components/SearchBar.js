import React from 'react';

export default class SearchBar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: '',
      darkMode: false,
      isShow: false,
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleEnter = this.handleEnter.bind(this);
  }

  handleChange(event) {
    this.setState({value: event.target.value});
  }

  handleEnter(event) {
    if (event.keyCode === 13) {
      const keyword = this.state.value.replace(/\s+/g, '-').toLowerCase();
      this.props.location.push(`/search/${keyword}`);
    }
  }

  render() {
    return (
      <div className="mb-40">
        <div className="form-group">
          <input
            placeholder="Search via GIPHY"
            className="form-control is-large is-search"
            value={ this.state.value }
            onChange={ this.handleChange }
            onKeyUp={ this.handleEnter } />
        </div>
      </div>
    );
  }
}