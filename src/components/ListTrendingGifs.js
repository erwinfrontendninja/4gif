import React from 'react';
import axios from 'axios';

class ListTrendingGifs extends React.Component {
  constructor(props) {
    super(props);
    this.state = { 
      gifs: {},
      isLoading: true,
      error: null,
    };
  }

  fetchGifs() {
    axios.get('/trending')
      .then((res) => this.setState({ gifs: res.data, isLoading: false }))
      .catch((error) => this.setState({ error, isLoading: false }));
  }

  componentDidMount() {
    this.fetchGifs();
  }

  infiniteScroll(event) {
    debugger;
  }

  render() {
    const { isLoading, gifs, error } = this.state;
    return (
      <div>
        <h1 className="mt-40 u-str-18">Trending Gifs</h1>
        <div className="flex">
          { error ? <p>{ error.message }</p> : null }
          { !isLoading
            ? (
              gifs.data.map((res) => {
                const { id, title, images } = res;
                const imageUrl = {
                  backgroundImage: `url(${images.preview_webp.url})`,
                };
                return (
                  <div key={ id } id={ id } className="flex-item flex-item--box" style={ imageUrl }>
                    <h4 className="u-reg-14">{ title }</h4>
                  </div>
                )
              })
            )
            : (
              <h3 className="u-reg-14">Loading...</h3>
            )
          }
        </div>
      </div>
    );
  }

}

export default ListTrendingGifs;