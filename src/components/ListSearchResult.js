import React from 'react';
import axios from 'axios';
import InfiniteScroll from 'react-infinite-scroller';

export default class ListSearchResult extends React.Component {
  constructor(props) {
    super(props);
    this.state = { 
      gifs: {},
      gifLists: [],
      isLoading: true,
      error: null,
      limit: 20,
      page: 0,
    };
    this.loadMore = this.loadMore.bind(this);
  }

  fetchResultGifs() {
    axios.get(`/search?q=${this.props.keyword}&limit=${this.state.limit}`)
      .then((res) => this.setState({ 
        gifs: res.data, 
        gifLists: res.data.data, 
        isLoading: false 
      }))
      .catch((error) => this.setState({ error, isLoading: false }));
  }

  componentDidMount() {
    this.fetchResultGifs();
  }

  componentDidUpdate(prevProps) {
    if (!(this.props.keyword === prevProps.keyword)) {
      this.fetchResultGifs();
    }
  }

  loadMore(e) {
    if (e !== this.state.page) {
      axios.get(`/search?q=${this.props.keyword}&limit=${this.state.limit}`)
        .then((res) => {
          this.setState({ 
            limit: this.state.limit + 10,
            gifLists: res.data.data,
            gifs: res.data,
            isLoading: false,
          })
        })
        .catch((error) => this.setState({ error, isLoading: false }));
    } 
  }

  render() {
    const { isLoading, gifs, error, gifLists } = this.state;
    return (
      <div>
        {
          !isLoading
          ? (
            <h4 className="u-reg-14">{ gifs.pagination.total_count } GIFs</h4>
          ) : ''
        } 
        <InfiniteScroll
          pageStart={ 0 }
          loadMore={ this.loadMore }
          hasMore={ true || false }
          loader={ <div className="loader" key={0}>Loading ...</div> }
          >
          <div className="flex">
            { error ? <p>{ error.message }</p> : null }
            { !isLoading
              ? (
                gifLists.map((res) => {
                  const { id, title, images } = res;
                  const imageUrl = {
                    backgroundImage: `url(${images.preview_webp.url})`,
                  };
                  return (
                    <div key={ id } id={ id } className="flex-item flex-item--box" style={ imageUrl }>
                      <h4 className="u-reg-14">{ title }</h4>
                    </div>
                  )
                })
              )
              : (
                <h3 className="u-reg-14">Loading...</h3>
              )
            }
          </div>
        </InfiniteScroll>
      </div>
    );
  }

}