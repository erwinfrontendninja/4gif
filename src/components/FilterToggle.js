import React from 'react';
import EventEmitter from './../Event';

class FilterToggle extends React.Component {
  constructor(props) {
    super(props);
    this.state = { checked: '' };
    this.toggleAction = this.toggleAction.bind(this);
  }

  toggleAction(event) {
    this.setState({ checked: event.target.checked });
    EventEmitter.emit('filterToggle', event.target.checked);
  }

  render() {
    return (
      <label className={ this.state.checked ? 'toggle is-checked' : 'toggle' }>
        <input type="checkbox" onChange={ this.toggleAction } hidden/>
        <div className="toggle-text u-str-18">
          <span>Trending</span>
          <span>Random</span>
        </div>
      </label>
    );
  }
}

export default FilterToggle;