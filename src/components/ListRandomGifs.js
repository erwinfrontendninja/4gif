import React from 'react';
import axios from 'axios';

class ListRandomGifs extends React.Component {
  constructor(props) {
    super(props);
    this.state = { 
      gif: {},
      isLoading: true,
      error: null,
    };
  }

  fetchGifs() {
    axios.get('/random')
      .then((res) => this.setState({ gif: res.data, isLoading: false }))
      .catch((error) => this.setState({ error, isLoading: false }));
  }

  componentDidMount() {
    this.fetchGifs();
  }

  render() {
    const { isLoading, gif, error } = this.state;
    const imageUrl = {
      backgroundImage: !isLoading ? `url(${gif.data.images.preview_webp.url})` : '',
    };
    return (
      <div>
        <h1 className="mt-40 u-str-18">Random Gifs</h1>
        <div className="flex">
          { error ? <p>{ error.message }</p> : null }
          { !isLoading
            ? (
              <div id={ gif.data.id } className="flex-item flex-item--box" style={ imageUrl }>
                <h4 className="u-reg-14">{ gif.data.title }</h4>
              </div>
            )
            : (
              <h3 className="u-reg-14">Loading...</h3>
            )
          }
        </div>
      </div>
    );
  }

}

export default ListRandomGifs;